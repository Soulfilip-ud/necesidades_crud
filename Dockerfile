FROM golang:1.12.1-alpine as base
RUN apk add --no-cache git
RUN apk add --no-cache build-base

RUN set -x \
    # go get bee
    && go get -u github.com/astaxie/beego \
    && go get -u github.com/beego/bee \
    && go get -u github.com/lib/pq 
WORKDIR /go/src
FROM base AS build

ENV API__BASE_DIR github.com/udistrital
ENV API_NAME necesidades_crud

WORKDIR /go/src/$API__BASE_DIR/$API_NAME

COPY . /go/src/$API__BASE_DIR/$API_NAME

RUN go get -v ./...

EXPOSE 8082

CMD ["sh", "-c", "bee run"]
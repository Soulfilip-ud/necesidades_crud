package main

import (
	_ "github.com/udistrital/necesidades_crud/routers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/lib/pq"
)

func main() {
	sqlconn := "postgres://" + beego.AppConfig.String("PGuser") + ":" + beego.AppConfig.String("PGpass") + "@" + beego.AppConfig.String("PGurls") + "/" + beego.AppConfig.String("PGdb") + "?sslmode=disable&search_path=" + beego.AppConfig.String("PGschemas") + ""
	if beego.AppConfig.String("runmode") == "AWS" {
		sqlconn = "postgres://" + beego.AppConfig.String("PGuserAWS") + ":" + beego.AppConfig.String("PGpassAWS") + "@" + beego.AppConfig.String("PGurlsAWS") + "/" + beego.AppConfig.String("PGdbAWS") + "?sslmode=disable&search_path=" + beego.AppConfig.String("PGschemas") + ""
	}
	orm.RegisterDataBase("default", "postgres", sqlconn)
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.Run()
}
